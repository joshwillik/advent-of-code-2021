import Util
import Data.List

main = runInputs 7 $ map (\fn -> show . fn . parseInput) [part1, part2]

part1 :: PuzzleInput -> Int
part1 = optimalBurn Linear

part2 :: PuzzleInput -> Int
part2 = optimalBurn Exponential

type PuzzleInput = [Int]
data BurnType = Linear | Exponential

parseInput :: String -> [Int]
parseInput "" = []
parseInput input = case eatWhile isDigit $ dropWhile (not . isDigit) input of
  ("", rest) -> parseInput rest
  (int, rest) -> read int : parseInput rest

isDigit :: Char -> Bool
isDigit c = c `elem` "1234567890"

eatWhile :: (Char -> Bool) -> String -> (String, String)
eatWhile fn input = (reverse found, rest) where
  (found, rest) = eatWhile' fn input ""

eatWhile' :: (Char -> Bool) -> String -> String -> (String, String)
eatWhile' _ "" buffer = (buffer, "")
eatWhile' fn (c:cs) buffer = case fn c of
  True -> eatWhile' fn cs (c:buffer)
  False -> (buffer, c:cs)

optimalBurn :: BurnType -> PuzzleInput -> Int
optimalBurn burnType positions = min lower higher where
  lower = localMinimum $ map burnCost [middle, middle-1 .. 0]
  higher = localMinimum $ map burnCost [middle, middle+1 .. ]
  burnCost to = sum $ map (fuelCost burnType to) positions
  middle = median positions

localMinimum :: [Int] -> Int
localMinimum (x:xs) = step x xs where
  step a [] = a
  step a (b:bs) = case compare a b of
                  LT -> a
                  _ -> step b bs

fuelCost :: BurnType -> Int -> Int -> Int
fuelCost Linear to from = abs $ from - to
fuelCost Exponential to from = triangular $ abs $ from - to

triangular :: Int -> Int
triangular 0 = 0
triangular n = n + (triangular $ n-1)

median :: [Int] -> Int
median list = (sort list) !! ((length list) `div` 2)
