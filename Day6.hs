import Util
import Data.List

main = runInputs 6 $ map (\fn -> show . fn . parseInput) [part1, part2]

type PuzzleInput = [Int]
parseInput :: String -> [Int]
parseInput input = ageGroups $ parseFish input where
  ageGroups ages = do
    i <- [0..8]
    return $ length $ filter (==i) ages

parseFish :: String -> [Int]
parseFish "" = []
parseFish (x:xs) = case x of
                    ',' -> rest
                    '\n' -> rest
                    v -> (read [v]) : rest
  where rest = parseFish xs

part1 :: PuzzleInput -> Int
part1 fish = sum $ populate fish 80

part2 :: PuzzleInput -> Int
part2 fish = sum $ populate fish 256

populate :: [Int] -> Int -> [Int]
populate fish 0 = fish
populate (reproducing:rest) days = populate population (days-1) where
  population = (updateAt 6 (+reproducing) rest) ++ [reproducing]

updateAt :: Int -> (a -> a) -> [a] -> [a]
updateAt 0 fn (first:rest) = fn first : rest
updateAt i fn (first:rest) = first : updateAt (i-1) fn rest
