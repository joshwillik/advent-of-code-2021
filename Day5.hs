import Util
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Data.Void
import Data.List

main = runInputs 5 $ map (\fn -> show . fn . parseInput) [part1, part2]

type Parser = Parsec Void String
type PuzzleInput = [Line]
type Line = (Pos, Pos)
type Pos = (Int, Int)
type Board = [[Cell]]
type Cell = (Pos, Int)

parseInput :: String -> PuzzleInput
parseInput x = case runParser puzzleInput "" x of
  Left _ -> []
  Right v -> v

puzzleInput :: Parser PuzzleInput
puzzleInput = many $ line <* eol

line :: Parser (Pos, Pos)
line = do
  from <- position
  string " -> "
  to <- position
  return (from, to)

position :: Parser (Int, Int)
position = do
  a <- int
  char ','
  b <- int
  return (a, b)

int :: Parser Int
int = read <$> (some $ oneOf "0123456789")

part1 :: PuzzleInput -> Int
part1 lines = length $ filter ((>1) . snd) $ frequency points where
  points = sortBy byPos $ concat $ map inflate $ filter straight lines

part2 :: PuzzleInput -> Int
part2 lines = length $ filter ((>1) . snd) $ frequency points where
  points = sortBy byPos $ concat $ map inflate lines

straight :: Line -> Bool
straight ((ax, ay), (bx, by)) = ax==bx || ay==by

inflate :: Line -> [Pos]
inflate ((ax, ay), (bx, by)) 
  | ax==bx && ay==by = [(ax, ay)]
  | otherwise = (ax, ay) : inflate ((towards bx ax, towards by ay), (bx, by))
    where towards b a = case compare a b of
                        LT -> a+1
                        GT -> a-1
                        EQ -> a
byPos :: Pos -> Pos -> Ordering
byPos (ax, ay) (bx, by) = case compare ax bx of
  EQ -> compare ay by
  GT -> GT
  LT -> LT

frequency :: Eq a => [a] -> [(a, Int)]
frequency = map (\x -> (head x, length x)) . group
