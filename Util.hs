module Util where
import Control.Monad
import System.Directory
import Data.List.Split

runInputs :: Int -> [(String -> String)] -> IO ()
runInputs day fns = do
    putStrLn $ "Day " ++ (show day) ++ ":"
    input <- readFile $ "./input/" ++ (show day) ++ ".txt"
    let runPart (part, fn) = (do
        putStrLn $ "Part " ++ show part ++ "> " ++ (fn input))
    mapM runPart $ zip [1..] fns
    return ()
