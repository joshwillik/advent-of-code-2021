import Util
import qualified Data.Map.Strict as Map
import Data.List
import Data.Function

main = runInputs 3 $ map (\fn -> show . fn . parseInput) [part1, part2]

data Frequency = Common | Rare

parseInput :: String -> [String]
parseInput = lines
 
part1 :: [String] -> Int
part1 input = gamma * epsilon
    where (gamma, epsilon) = energyLevels $ transpose input

part2 :: [String] -> Int
part2 input = oxygen * co2
    where (oxygen, co2) = lifeSupportRating input

energyLevels :: [String] -> (Int, Int)
energyLevels columns = (gamma, epsilon)
    where
        gamma = bin2dec $ map fst charFrequency
        epsilon = bin2dec $ map snd charFrequency
        charFrequency = map frequencyPair columns

lifeSupportRating :: [String] -> (Int, Int)
lifeSupportRating inputs = (oxygen, co2)
    where
        extract freq = bin2dec $ filterReadings freq inputs
        oxygen = extract Common
        co2 = extract Rare

filterReadings :: Frequency -> [String] -> String
filterReadings = filterReadings' 0

filterReadings' :: Int -> Frequency -> [String] -> String
filterReadings' _ _ (x:[]) = x
filterReadings' index freq values = filterReadings' (index+1) freq matches
    where
        matches = filter (\x -> (x !! index) == selectedChar) values
        selectedChar = case freq of
            Common -> common
            Rare -> rare
        (common, rare) = frequencyPair $ (transpose values) !! index

frequencyPair :: String -> (Char, Char)
frequencyPair str = (common, rare)
    where
        counts = chars str
        sorter = compare `on` snd
        common = fst $ maximumBy sorter counts
        rare = fst $ minimumBy sorter counts

chars :: String -> [(Char, Int)]
chars str = Map.toList $ chars' str Map.empty

chars' :: String -> Map.Map Char Int -> Map.Map Char Int
chars' (x:xs) m = Map.insertWith (+) x 1 (chars' xs m)
chars' [] m = m

bin2dec :: String -> Int
bin2dec str = bin2dec' 1 $ reverse str

bin2dec' :: Int -> String -> Int
bin2dec' n (x:xs) = (read [x])*n + (bin2dec' (n*2) xs)
bin2dec' _ "" = 0
