{-# LANGUAGE TupleSections #-}
import Util
import Data.List
import qualified Data.Map.Strict as Map

main = runInputs 8 $ map (\fn -> show . fn . parseInput) [part1]

type PuzzleInput = [Reading]
type Reading = ([[Signal]], [[Signal]])

part1 :: PuzzleInput -> Either String Int
part1 readings = do 
  decoded <- mapM solveReading readings
  return $ length $ filter (intersects [1, 4, 7, 8]) decoded

type Signal = Char
data Segment = Top |
               TopRight | TopLeft |
               Middle |
               BottomRight | BottomLeft |
               Bottom
  deriving (Eq, Show, Ord)

type SegmentMap = Map.Map Segment [Signal]
type SignalMap = Map.Map Signal Segment

solveReading :: Reading -> Either String [Int]
solveReading (samples, outputs) = case solveSignalMap (samples ++ outputs) of
  Right map' -> Right $ map (decode map') outputs
  Left err -> Left err 

decode :: SignalMap -> [Signal] -> Int
decode map' signals = fromSegments segments where 
  segments = map (map' Map.!) signals

fromSegments :: [Segment] -> Int
fromSegments s = out where
  out = case find isMatch [1..9] of
    Just v -> v
    Nothing -> error "Invalid segment map"
  isMatch digit = (length $ s `intersect` segments digit) == 0


segments :: Int -> [Segment]
segments 0 = [Top, TopRight, TopLeft, BottomRight, Bottom]
segments 1 = [TopRight, BottomRight]
segments 2 = [Top, TopLeft, Middle, BottomRight, Bottom]
segments 3 = [Top, TopRight, Middle, BottomRight, Bottom]
segments 4 = [TopLeft, TopRight, Middle, BottomRight]
segments 5 = [Top, TopLeft, Middle, BottomRight, Bottom]
segments 6 = [Top, TopLeft, Middle, BottomLeft, BottomRight, Bottom]
segments 7 = [Top, TopLeft, BottomLeft]
segments 8 = [Top, TopRight, TopLeft, Middle, BottomLeft, BottomRight, Bottom]
segments 9 = [Top, TopRight, TopLeft, Middle, BottomRight, Bottom]

parseInput :: String -> PuzzleInput
parseInput input = map parseLine $ lines input

parseLine :: String -> ([[Signal]], [[Signal]])
parseLine line = (samples, drop 1 outputs) where
  (samples, outputs) = span (/=['|']) letters
  letters = words line

defaultKnowledge :: SegmentMap
defaultKnowledge = Map.fromList $ map (, "abcdefg") segments where
  segments = [Top, TopRight, TopLeft, Middle, BottomLeft, BottomRight, Bottom]

solveSignalMap :: [[Signal]] -> Either String SignalMap
solveSignalMap samples = buildSignalMap $ loopApply step defaultKnowledge where
  step map' = foldr updateSegmentMap map' samples

buildSignalMap :: SegmentMap -> Either String SignalMap
buildSignalMap map'
  | isSolved map' =
    let entries = map (\(k, v) -> (head v, k)) $ Map.toList map'
    in Right $ Map.fromList entries
  | otherwise = Left "Segment map isn't unambigrious"

isSolved :: SegmentMap -> Bool
isSolved map' = foldr (&&) True $ map (\x -> length x==1) $ Map.elems map'

loopApply :: Eq a => (a -> a) -> a -> a
loopApply fn start = if next==start then start else loopApply fn next where
  next = fn start

updateSegmentMap :: [Signal] -> SegmentMap -> SegmentMap
updateSegmentMap [] map' = map'
updateSegmentMap signals map' = case matchDigit map' signals of
  Just digit -> Map.mapWithKey alter map' where
    segments' = segments digit
    alter k v = if k `elem` segments' then union signals v else v
  Nothing -> map'

matchDigit :: SegmentMap -> [Signal] -> Maybe Int
matchDigit _ _ = Nothing -- CONTINUE: complete this function

bySegments :: [Segment] -> [Int]
bySegments segments = case length segments of
  0 -> []
  1 -> []
  2 -> [1]
  3 -> [7]
  4 -> [4]
  5 -> [2, 3, 5, 9]
  6 -> [6, 0]
  7 -> [8]
  _ -> []

intersects :: Eq a => [a] -> [a] -> Bool
intersects [] _ = False
intersects (x:xs) vs = x `elem` vs || intersects xs vs
