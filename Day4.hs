import Util
import Prelude hiding (flip)
import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Void
import Data.List

main = runInputs 4 $ map (\fn -> show . fn . parseInput) [part1, part2]

type Parser = Parsec Void String
type PuzzleInput = ([Int], [Board])
type Board = [[(Int, Bool)]]

parseInput :: String -> PuzzleInput
parseInput x = case runParser puzzleInput "" x of
  Left _ -> ([], [])
  Right v -> v

puzzleInput :: Parser PuzzleInput
puzzleInput = pure (,) <*> inputNumbers <*> boards

inputNumbers :: Parser [Int]
inputNumbers = int `sepBy` (char ',') <* string "\n\n"

boards :: Parser [Board]
boards = board `sepBy` eol

board :: Parser Board
board = some $ numberLine <* eol

numberLine :: Parser [(Int, Bool)]
numberLine = map (\x -> (x, False)) <$> (int `sepBy1` hspace1)

int :: Parser Int
int = read <$> (hspace *> (some $ oneOf "0123456789"))
 
part1 :: PuzzleInput -> Int
part1 (inputs, boards) = sumUnmarked board * number where
  (number, board) = head $ playGames boards inputs

part2 :: PuzzleInput -> Int
part2 (inputs, boards) = sumUnmarked board * number where
  (number, board) = last $ playGames boards inputs

sumUnmarked :: Board -> Int
sumUnmarked board = foldr (+) 0 (map fst inactives) where
  inactives = concat $ map (filter isOff) board
  isOff (_, active) = active==False

playGames :: [Board] -> [Int] -> [(Int, Board)]
playGames boards [] = []
playGames [] numbers = []
playGames boards (x:xs) = chain where
  chain = case find isWinner boards' of
    Just winner -> (x, winner) : playGames losers (x:xs)
    Nothing -> playGames boards' xs
  losers = filter (not . isWinner) boards'
  boards' = map (updateBoard x) boards

updateBoard :: Int -> Board -> Board
updateBoard v b = map (map replace) b where
  replace (v', isOn) = (v', v==v' || isOn)

isWinner :: Board -> Bool
isWinner b = foldr (||) False $ map (foldr (&&) True) options where
  options = map (map snd) $ boardRows b

testBoard :: [[(Int, Bool)]]
testBoard =
  [
    [(1, False), (2, True), (3, False)],
    [(4, False), (5, True), (6, False)],
    [(7, False), (8, True), (9, False)]
  ]
boardRows :: Board -> [[(Int, Bool)]]
boardRows b = concat [b, columns b] where
  columns b = transpose b

flip :: [[a]] -> [[a]]
flip = map reverse
