import Util

main = runInputs 1 $ map (\fn -> show . fn . parseInput) [part1, part2]

parseInput :: String -> [Int]
parseInput input = map read $ lines input

part1 :: [Int] -> Int
part1 input = length $ filter isDescent windows
    where windows = chunks 2 input

part2 :: [Int] -> Int
part2 input = length $ filter isDescent $ windows
    where windows = chunks 2 $ map sum $ chunks 3 input

chunks :: Int -> [Int] -> [[Int]]
chunks n list
    | (length list >= n) = (take n list) : (chunks n $ drop 1 list)
    | otherwise = []

isDescent :: [Int] -> Bool
isDescent (a:b:[]) = a<b
