import Util

main = runInputs 2 $ map (\fn -> show . fn . parseInput) [part1, part2]

data Direction = Forward | Up | Down
type Movement = (Int, Direction)
type Position = (Int, Int)
type Aim = Int

parseInput :: String -> [Movement]
parseInput input = map (parseCommand . words) $ lines input

parseCommand :: [String] -> Movement
parseCommand (direction:distance:[]) = (distance', direction')
    where
        distance' = read distance
        direction' = case direction of
            "forward" -> Forward
            "up" -> Up
            "down" -> Down
            _ -> error $ "Didn't understand direction in " ++ direction
parseCommand line = error $ "Didn't understand the line: " ++ (unwords line)

part1 :: [Movement] -> Int
part1 input = x*y
    where (x, y) = foldl applyMovement (0, 0) input

applyMovement :: Position -> Movement -> Position
applyMovement (x, y) (distance, direction) = case direction of
    Up -> (x, y-distance)
    Down -> (x, y+distance)
    Forward -> (x+distance, y)

part2 :: [Movement] -> Int
part2 input = x*y
    where ((x, y), _) = foldl applyMovement' ((0, 0), 0) input

applyMovement' :: (Position, Aim) -> Movement -> (Position, Aim)
applyMovement' ((x, y), aim) (distance, direction) = case direction of
    Up -> ((x, y), aim-distance)
    Down -> ((x, y), aim+distance)
    Forward -> ((x+distance, y+distance*aim), aim)
